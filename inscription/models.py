from __future__ import unicode_literals

import uuid

from django.db import models
from django.template.defaultfilters import slugify
from django.urls import reverse

from chunked_upload.models import ChunkedUpload

from .validators import FileValidator


class InscriptionValidManager(models.Manager):
    ''' Filtro para retornar inscrições válidas '''

    def get_queryset(self):
        return super().get_queryset().filter(
            status=Inscription.VALIDA).order_by('grupo', 'espetaculo')


class Inscription(models.Model):

    def __str__(self):
        return '%s - %s - %s' % (
            self.grupo,
            self.espetaculo,
            self.MOSTRA_OPC[self.mostra - 1][1])

    def get_absolute_url(self):
        kwargs = {'slug': self.slug}
        return reverse('mostra-regional-espetaculo', kwargs=kwargs)

    def get_upload_to_file(self, filename):
        ext = filename[-3:].lower()

        return 'inscription/%s-%s.%s' % (
            uuid.uuid4(),
            slugify(filename.strip()),
            ext)

    def save(self, *args, **kwargs):
        if not self.id:
            super().save(*args, **kwargs)

        if not self.slug:
            self.slug = slugify(self.__str__().strip())

        super().save(*args, **kwargs)

    class Meta:
        verbose_name = "Espetáculo inscrito"
        verbose_name_plural = "Espetáculos inscritos"

    REGIONAL = 1
    ESTADUAL = 2
    NACIONAL = 3

    MOSTRA_OPC = (
        (REGIONAL, 'REGIONAL'),
        (ESTADUAL, 'ESTADUAL'),
        (NACIONAL, 'NACIONAL'),
    )

    INVALIDA = 1
    VALIDA = 2
    SELECIONADA = 3

    MOSTRA_STATUS = (
        (INVALIDA, 'Inválida'),
        (VALIDA, 'Válida'),
        (SELECIONADA, 'Selecionada'),
    )

    """ Managers """
    objects = models.Manager()
    valid = InscriptionValidManager()

    """ Fields """
    grupo = models.CharField('Nome do grupo', max_length=200)
    espetaculo = models.CharField('Nome do espetáculo', max_length=200)
    mostra = models.PositiveSmallIntegerField('Mostra', choices=MOSTRA_OPC)
    slug = models.SlugField(max_length=200, null=True, blank=True)
    release = models.TextField('Release')
    sinopse = models.TextField('Sinopse')
    ficha_tecnica = models.TextField('Ficha técnica')
    historico = models.TextField('Histórico da peça/ Grupo')

    pesquisa = models.TextField('Proposta de pesquisa e encenação')

    classificacao = models.CharField('Classificação indicativa', max_length=50)
    duracao = models.CharField('Duração do espetáculo', max_length=50)

    email = models.EmailField(
        'E-mail',
        help_text='Entraremos em contato por este canal')

    email_optional = models.EmailField(
        'E-mail opcional',
        null=True, blank=True)

    contato = models.TextField(
        'Contatos do grupo pra divulgação',
        help_text='Site, blog, e-mail, redes sociais, etc')

    rider_tecnico = models.TextField(
        'Rider Técnico',
        help_text='Especificar necessidades técnicas, mapa de som e luz')

    links = models.TextField(
        'Links de vídeo do espetáculo',
        help_text='Se o link do vídeo for fechado enviar senha. Em caso de espetáculos \
        inéditos, deverão ser encaminhados vídeos de ensaio')

    tempo_des_montagem = models.CharField(
        'Tempo de montagem e desmontagem',
        max_length=100)

    # PDF!!!
    autorizacao = models.FileField(
        'Autorização',
        help_text='Autorização SBAT ou liberação do autor (.PDF)',
        upload_to=get_upload_to_file,
        validators=[FileValidator(
            allowed_mimetypes=(
                'application/pdf',),
            max_size=4 * 1024 * 1024), ],)

    # PDF!!!
    clipping = models.FileField(
        'Clipping, materiais adicionais',
        help_text='Selecione um arquivo PDF',
        upload_to=get_upload_to_file,
        validators=[FileValidator(
            allowed_mimetypes=(
                'application/pdf',),
            max_size=4 * 1024 * 1024), ],)

    status = models.PositiveSmallIntegerField(
        'Situação', choices=MOSTRA_STATUS, default=VALIDA)

    created_at = models.DateTimeField('Criado em', auto_now_add=True)
    updated_at = models.DateTimeField('Atualizado em', auto_now=True)

    obs = models.TextField(
        'Observações',
        help_text='Observações internas',
        null=True, blank=True)


class InscriptionImages(models.Model):

    """
    TODO:
        - plugin para versões otimizadas das imagens
    """

    def __str__(self):
        return '%s' % (self.image)

    def get_upload_to_image(self, filename):
        ext = filename[-3:].lower()
        if ext == 'peg':
            ext = 'jpeg'

        return 'inscription/%s/%s-%s.%s' % (
            self.inscription_id,
            uuid.uuid4(),
            slugify(filename.strip()),
            ext)

    class Meta:
        verbose_name = "Fotos da inscrição"
        verbose_name_plural = "Fotos da inscrição"

    inscription = models.ForeignKey(
        Inscription,
        null=True, blank=True,
        verbose_name=u"Inscrição",
        on_delete=models.CASCADE)

    image = models.ImageField(
        'Imagem',
        upload_to=get_upload_to_image,
        max_length=300)

    image_opt = models.ImageField(
        'Imagem Otimizada',
        upload_to=get_upload_to_image,
        max_length=300,
        null=True, blank=True)


class MyChunkedUpload(ChunkedUpload):
    pass


# Override the default ChunkedUpload to make the `user` field nullable
MyChunkedUpload._meta.get_field('user').null = True
