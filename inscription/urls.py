from django.conf.urls import url

from inscription import views

urlpatterns = [

    #####################
    # INSCRIPTION
    #####################

    # /
    url(r'^$', views.Inscription.as_view(), name='form-new'),

    # /success
    url(
        r'^success/(?P<pk>[0-9]+)/$',
        views.InscriptionSuccess.as_view(),
        name='form-success'),


    ############################
    # CURADORIA MOSTRA REGIONAL
    ############################

    # /mostra/regional
    url(
        r'^mostra/regional/$',
        views.InscriptionMostraRegional.as_view(),
        name='mostra-regional'),

    # /mostra/regional/espetaculo-da-mostra-regional
    url(
        r'^mostra/regional/(?P<slug>[\w-]+)$',
        views.InscriptionMostraRegionalEspetaculo.as_view(),
        name='mostra-regional-espetaculo'),
]


############################
# chunked_upload urls
############################

urlpatterns += [

    url(r'^chunked$',
        views.ChunkedUploadDemo.as_view(), name='chunked_upload'),

    url(r'^chunked/api/chunked_upload/?$',
        views.MyChunkedUploadView.as_view(), name='api_chunked_upload'),

    url(r'^chunked/api/chunked_upload_complete/?$',
        views.MyChunkedUploadCompleteView.as_view(),
        name='api_chunked_upload_complete'),
]
