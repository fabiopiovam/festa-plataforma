from django.contrib import admin

from .models import Inscription, InscriptionImages


def make_valid(modeladmin, request, queryset):
    queryset.update(status=Inscription.VALIDA)


make_valid.short_description = "Marcar selecionadas como VÁLIDA"


def make_invalid(modeladmin, request, queryset):
    queryset.update(status=Inscription.INVALIDA)


make_invalid.short_description = "Marcar selecionadas como INVÁLIDA"


def make_approved(modeladmin, request, queryset):
    queryset.update(status=Inscription.SELECIONADA)


make_approved.short_description = "Marcar selecionadas como SELECIONADA"


class InscriptionImagesInline(admin.TabularInline):
    model = InscriptionImages
    extra = 0


class InscriptionAdmin(admin.ModelAdmin):
    list_display = ('grupo', 'espetaculo', 'mostra', 'status', 'created_at')
    search_fields = ['grupo', 'espetaculo', ]
    list_filter = ['status', 'mostra']
    date_hierarchy = 'created_at'

    exclude = ('slug',)

    actions = [make_valid, make_invalid, make_approved]

    inlines = [
        InscriptionImagesInline,
    ]


admin.site.register(Inscription, InscriptionAdmin)
