import mimetypes
import re

from django.core.mail import mail_managers, send_mail as sendmail
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView
from django.views.generic.detail import DetailView
from django.views.generic import ListView
from django.urls import reverse_lazy
from django.shortcuts import redirect
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from chunked_upload.views import ChunkedUploadView, ChunkedUploadCompleteView
from chunked_upload.exceptions import ChunkedUploadError
from chunked_upload.constants import http_status

from .models import Inscription as Inscr, MyChunkedUpload, InscriptionImages
from .forms import InscriptionForm


class Inscription(CreateView):

    model = Inscr
    form_class = InscriptionForm
    template_name = 'inscription/form_unavailable.html'
    success_url = reverse_lazy('chunked_upload')

    def send_mail(self):

        post = self.request.POST

        mostra_opc = dict(self.model.MOSTRA_OPC)
        mostra_sel = int(post['mostra'])

        subject = 'Inscrição - %s - %s - %s' % (
            post['grupo'],
            post['espetaculo'],
            mostra_opc[mostra_sel])

        change_url = reverse_lazy('admin:inscription_inscription_change',
                                  args=(self.object.id,))
        url = str(self.request.build_absolute_uri(str(change_url)))

        msg = """
Ficha de inscrição

Grupo: %s
Espetáculo: %s
Mostra: %s

Release: %s

Sinopse: %s

Histórico: %s

Ficha técnica: %s

Proposta de pesquisa: %s

Contato: %s

%s

        """ % (post['grupo'], post['espetaculo'],
               mostra_opc[mostra_sel], post['release'],
               post['sinopse'], post['historico'], post['ficha_tecnica'],
               post['pesquisa'], post['contato'], url)

        try:
            mail_managers(subject, msg, fail_silently=False)
        except Exception as e:
            if settings.DEBUG:
                raise
            return False

        return True

    def send_mail_mostra_regional(self):

        post = self.request.POST

        contact_list = re.compile(",|\s|;").split(post['contato'])
        contact_list = [mail for mail in contact_list if re.match('(\w+@\w+)', mail)]

        subject = 'Inscrição e informações importantes'

        msg = """
Parabéns, sua inscrição foi reaizada com sucesso!!


Art. 5º-  Mostra Regional da Baixada Santista

a )   Seguindo o modelo de curadoria coletiva proposto pelo Movimento Teatral da Baixada Santista, a seleção será realizada em 3 etapas:


ETAPA 1- Atividade formativa voltada à curadoria com participação de pelo menos 1 (hum) integrante do grupo inscrito no dia 23 de junho; domingo às 17h na Cadeia Velha(opção 1) ou no Teatro Guarany (Opção 2) com Alexandre Mate.


ETAPA 2- Atividade formativa voltada à curadoria com participação de pelo menos 1 (hum) integrante do grupo inscrito no dia 30 de junho; domingo às 17h na Cadeia Velha(opção 1) ou no Teatro Guarany (Opção 2) com Kil Abreu.


ETAPA 3 – Integrante que participou das atividades formativas poderá votar pelo grupo que representa em reunião de curadoria da mostra regional no dia 14 de Julho, domingo às 14h, no Centro Cultural Cadeia Velha, após o fim das inscrições, em comissão formada por um integrante de cada grupo inscrito e a organização do festival, não sendo válido avaliar o seu próprio trabalho - sistema de voto aberto e justificado durante a reunião.

        """

        try:
            sendmail(subject, msg, settings.DEFAULT_FROM_EMAIL,
                     contact_list, fail_silently=False)
        except Exception as e:
            if settings.DEBUG:
                raise
            return False

        return True

    def form_valid(self, form):
        self.object = form.save()

        self.send_mail()
        self.object.save()

        if int(self.object.mostra) == self.model.REGIONAL:
            self.send_mail_mostra_regional()

        self.request.session['count_chunk'] = 0
        self.request.session['inscription_id'] = self.object.id

        return super().form_valid(form)


class ChunkedUploadDemo(TemplateView):
    template_name = 'chunked_demo/chunked_upload_demo.html'

    def dispatch(self, request, *args, **kwargs):
        if not request.session.get('inscription_id', False):
            return redirect('form-new')

        return super().dispatch(request, *args, **kwargs)


class MyChunkedUploadView(ChunkedUploadView):

    model = MyChunkedUpload
    field_name = 'the_file'

    mime_message = _("MIME type '%(mimetype)s' is not valid. \
        Allowed types are: %(allowed_mimetypes)s.")
    allowed_mimetypes = (
        'image/png',
        'image/jpeg')

    def check_permissions(self, request):
        # Allow non authenticated users to make uploads
        pass

    def is_valid_chunked_upload(self, chunked_upload):

        mimetype = mimetypes.guess_type(chunked_upload.filename)[0]
        if self.allowed_mimetypes and not mimetype in self.allowed_mimetypes:
            message = self.mime_message % {
                'mimetype': mimetype,
                'allowed_mimetypes': ', '.join(self.allowed_mimetypes)
            }

            raise ChunkedUploadError(status=http_status.HTTP_400_BAD_REQUEST,
                                     detail=message)

        return super().is_valid_chunked_upload(chunked_upload)



class MyChunkedUploadCompleteView(ChunkedUploadCompleteView):

    model = MyChunkedUpload
    do_md5_check = False

    def check_permissions(self, request):
        # Allow non authenticated users to make uploads
        pass

    def on_completion(self, uploaded_file, request):
        # Do something with the uploaded file. E.g.:
        # * Store the uploaded file on another model:
        InscriptionImages.objects.create(
            inscription_id=request.session.get('inscription_id'),
            image=uploaded_file)

        self.request.session['count_chunk'] += 1
        # * Pass it as an argument to a function:
        # function_that_process_file(uploaded_file)

    def get_response_data(self, chunked_upload, request):

        return {'message': ("You successfully uploaded '%s' (%s bytes)!" %
                            (chunked_upload.filename, chunked_upload.offset))}


class InscriptionSuccess(DetailView):
    model = Inscr
    context_object_name = 'obj'
    template_name = 'inscription/form_success.html'

    def send_mail(self):
        obj = self.model.objects.get(id=self.request.session['inscription_id'])
        subject = 'Fotos da inscrição - %s' % obj.__str__()

        change_url = reverse_lazy(
            'admin:inscription_inscription_change',
            args=(self.request.session['inscription_id'],))

        url = str(self.request.build_absolute_uri(str(change_url)))

        msg = """
Fotos da inscrição

%s

        """ % (url, )

        try:
            mail_managers(subject, msg, fail_silently=False)
        except Exception as e:
            if settings.DEBUG:
                raise
            return False

        return True

    def dispatch(self, request, *args, **kwargs):

        if not request.session.get('inscription_id', False):
            return redirect('form-new')

        send = self.send_mail()

        del self.request.session['count_chunk']
        del self.request.session['inscription_id']

        # GARBAGE COLLECTION ON CHUNKED

        return super().dispatch(request, *args, **kwargs)


class InscriptionMostraRegional(ListView):

    template_name = 'inscription/mostra_regional.html'
    context_object_name = 'inscriptions'

    def get_queryset(self):
        return Inscr.valid.filter(mostra=Inscr.REGIONAL)


class InscriptionMostraRegionalEspetaculo(DetailView):

    template_name = 'inscription/mostra_regional_espetaculo.html'
    context_object_name = 'i'

    def get_queryset(self):
        return Inscr.valid.all()
