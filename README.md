# Festival Santista de Teatro - FESTA

Plataforma de inscrição

Disponível [aqui](http://festa61.movase.info/)

# Requerimentos

* [python 3](https://www.python.org/downloads/) `sudo apt install python3`

* [virtualenv](https://virtualenv.pypa.io/en/latest/installation/) `sudo apt install python3-virtualenv`

* [pip](https://pip.pypa.io/en/stable/installing/) `sudo apt install python3-pip`

* [postgresql 9.6+](https://www.postgresql.org/download/) `sudo apt install postgresql`

* [pgadmin3](https://www.pgadmin.org/download/) `sudo apt install pgadmin3`

# Instalação

* clonar o projeto: `git clone https://gitlab.com/laborautonomo/festa-plataforma.git`

* no diretório do projeto: `cd festa-plataforma`

* criar ambiente virtual para a instação dos pacotes python: `virtualenv -p python3 venv`

* acessar o novo ambiente virtual: `source venv/bin/activate`

* instalar requerimentos: `pip install -r requirements.txt`

# Configuração

* crie o arquivo de configurações local `core/settings_local.py`
* adicione o seguinte conteúdo, fazendo as devidas configurações segundo seu ambiente

  ```python
  # SECURITY WARNING: don't run with debug turned on in production!
  LOCAL = True
  DEBUG = True

  SECRET_KEY = '*(!%85o08m++)8mw_u)7hx3)h!ukf86erj)&%t^%&)kup!k$vd'

  ALLOWED_HOSTS = ["*"]

  EMAIL_HOST          = ''
  EMAIL_HOST_USER     = ''
  EMAIL_HOST_PASSWORD = ''
  EMAIL_PORT          = ''
  EMAIL_USE_TLS       = True

  EMAIL_SUBJECT_PREFIX= '[FESTA 61] '
  SERVER_EMAIL        = EMAIL_HOST_USER
  DEFAULT_FROM_EMAIL  = ''

  ADMINS = (('admin', DEFAULT_FROM_EMAIL),)

  MANAGERS = (('FESTA - Organizacao', ''),)

  # Show message in terminal instead send mail. Set only in dev env
  EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

  SESSION_COOKIE_NAME     = 'local.festa-plataforma' # Cookie name. This can be whatever you want.
  SECURE_SSL_REDIRECT     = False
  #SESSION_COOKIE_SECURE   = True
  #CSRF_COOKIE_SECURE      = True

  DATABASES = {
      'default': {
      'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
      'NAME': 'festa61',                      # Or path to database file if using sqlite3.
      # The following settings are not used with sqlite3:
      'USER': 'postgres',
      'PASSWORD': '',
      'HOST': 'localhost',                      # Empty for localhost through domain sockets or           '127.0.0.1' for localhost through TCP.
      'PORT': '5432',                      # Set to empty string for default.
      }
  }
  ```

* criar base de dados `festa61` no postgresql. Veja as instruções de como
* sincronizar base de dados: `python manage.py migrate`
* subir servidor de teste: `python manage.py runserver`

